# Vision Por Computadoras 2022 UTN FRC

Repositorio de los ejercicios de la materia de Vision por computadoras.

Requejo Agustín. UTN FRC 2022.

# Archivos

En la direccion __ejercicios/__ se encuentran las resoluciones de los ejercicios en _Python_

En la direccion __ejercicios/files/__ se encuentran los archivos utilizados en las resoluciones de los ejercicios
