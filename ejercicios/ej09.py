from string import hexdigits
import cv2
import numpy as np
import math as ma

#Dimensiones de la puerta: 0.83m x 2.04m
#Dimensiones de la ventana de la izquierda: 0.53m x 1.16m
#Dimensiones de la ventana de la derecha: 0.6m x 0.91m
#Dimensiones del calefactor: 0.34m x 0.60m
#Dimensiones del tomacorrientes: 0.13m x 0.08m

img_path = "files/ej9_img.jpg"
orig = cv2.imread(img_path)

def persp():
    '''
        Esta función realiza la transformación perspectiva de la imagen.
        No necesitamos entradas ya que utilizamos variables globales (mal)
        (mejor explicado en el ej8)
    '''
    global img, cache, oh, ow, out_img, out_pts_pers, hardcode
    global measure_height, measure_width

    despl_x = -180
    x_min= out_pts_pers[0][0] + despl_x
    x_max= out_pts_pers[3][0] + despl_x
    despl_y = 100
    y_min= out_pts_pers[0][1] + despl_y
    y_max= out_pts_pers[3][1] + despl_y

    #in_pts = np.float32([[0,0], [ow-1,0], [0,oh-1], [ow-1,oh-1]])
    in_pts = np.float32([out_pts_pers[0], out_pts_pers[1], out_pts_pers[2], out_pts_pers[3]])
    out_pts = np.float32([[x_min,y_min],[x_max,y_min],[x_min,y_max],[x_max,y_max]])
    M = cv2.getPerspectiveTransform(in_pts,out_pts)
    
    #Se ingresa el tamaño de lo medido (en el caso de que no este hardcodeado el ancho y el alto de lo medido)
    if not hardcode:
        measure_height = float(input('==> Ingrese el valor "h" de altura: '))
        measure_width = float(input('==> Ingrese el valor "w" de ancho: '))
    
    #Se parametriza lo medido de metros a metros por pixel para poder hacer las mediciones en la imagen
    measure_height = measure_height/abs(y_min-y_max)
    measure_width = measure_width/abs(x_min-x_max)
    
    out_img = cv2.warpPerspective(orig, M, (ow+200,oh+200))
    img = out_img.copy()
    cache = img.copy()

img = orig.copy()
cache = img.copy()
count_pers=4
dist_pts=[]
count_dist=2
dist_pts_2=[]
count_dist_2=2
oh, ow, och = orig.shape
draw = True

#Si desea cambiar la imagen y sus dimensiones, cambie la variable "hardcode" a False.
hardcode = True
if hardcode:
    mode_pers = False
    out_pts_pers = [[529, 72], [762-1, 108], [527, 759], [761-3, 715]]
    measure_height = 2.04
    measure_width = 0.83
    persp()
else:
    mode_pers = True
    out_pts_pers = []
    read_lengths = True

def rect_dist(event , x , y , flags , param):
    global img, cache, dist_pts, count_dist, draw, count_pers, out_pts_pers, mode_pers, measure_height, measure_width
    if event == cv2.EVENT_LBUTTONDOWN:
        draw = True
        if mode_pers:
            #Parecida a la de los ejercicios anteriores, va agregando los puntos a un array hasta tener los necesarios y despues llama a la función.
            #Esta de aqui son los puntos de ingreso para la "rectificación" (por transf perspectiva) de la imagen.
            if (count_pers > 0):
                out_pts_pers.append([x,y])
                count_pers-=1
                cv2.circle(img, (x,y), 5, (0,255,0), -1)
                if (count_pers==2):
                    cv2.line(img,tuple(out_pts_pers[0]),tuple(out_pts_pers[1]),(0,0,255),1)
                    cv2.putText(img, f"w", (int(min(out_pts_pers[0][0],out_pts_pers[1][0])+abs(out_pts_pers[0][0]-out_pts_pers[1][0])//2),int(min(out_pts_pers[0][1],out_pts_pers[1][1])+abs(out_pts_pers[0][1]-out_pts_pers[1][1])//2)), cv2.FONT_HERSHEY_DUPLEX, 1, (0,0,255))
                elif(count_pers==1):
                    cv2.line(img,tuple(out_pts_pers[0]),tuple(out_pts_pers[2]),(0,0,255),1)
                    cv2.putText(img, f"h", (int(min(out_pts_pers[0][0],out_pts_pers[2][0])+abs(out_pts_pers[0][0]-out_pts_pers[2][0])//2),int(min(out_pts_pers[0][1],out_pts_pers[2][1])+abs(out_pts_pers[0][1]-out_pts_pers[2][1])//2)), cv2.FONT_HERSHEY_DUPLEX, 1, (0,0,255))
                elif (count_pers == 0):
                    cv2.line(img,tuple(out_pts_pers[3]),tuple(out_pts_pers[1]),(0,0,255),1)
                    cv2.line(img,tuple(out_pts_pers[2]),tuple(out_pts_pers[3]),(0,0,255),1)
                    cache = img.copy()
                    cv2.imshow('image', img)
                    mode_pers=False
                    persp()
                    img = out_img.copy()
                    print(out_pts_pers)
                cache = img.copy()
        else:
            #Al hacer click dos veces, se calcula la distancia entre los dos puntos, y se muestra por pantalla y por consola.
            if (count_dist > 0):
                dist_pts.append([x,y])
                count_dist-=1
                cv2.circle(img, (x,y), 5, (0,255,0), -1)
                if (count_dist == 0):
                    cv2.line(img, tuple(dist_pts[0]), tuple(dist_pts[1]), (0,0,255), 1)
                    dist_x = abs(dist_pts[0][0] - dist_pts[1][0]) 
                    dist_y = abs(dist_pts[0][1] - dist_pts[1][1]) 
                    dist_cm = ma.hypot(dist_x * measure_width, dist_y * measure_height)
                    cv2.putText(img, f"{dist_cm:.2f}", (int(min(dist_pts[0][0],dist_pts[1][0])+dist_x//3),int(min(dist_pts[0][1],dist_pts[1][1])+dist_y//3)), cv2.FONT_HERSHEY_PLAIN, 2, (0,0,255), 1,cv2.LINE_AA)
                    print(f"=>La distancia entre los puntos {dist_pts[0]} y {dist_pts[1]} es de: {dist_cm:.5f}cm")
                cache = img.copy()
            
    elif event == cv2.EVENT_MOUSEMOVE:
        img = cache.copy()
        cache = img.copy()
        cv2.circle(img, (x,y), 5, (0,255,255), -1)
        draw = True

if hardcode:
    #Aqui se hardcodea el pedazo de imagen seleccionado por mi
    #Dimensiones -> Alto 2.04m, Ancho 0.83m
    img_temp = orig.copy()
    cv2.line(img_temp,tuple(out_pts_pers[0]),tuple(out_pts_pers[1]),(0,0,255),1)
    cv2.putText(img_temp, f"w=0.83m", (int(min(out_pts_pers[0][0],out_pts_pers[1][0])+abs(out_pts_pers[0][0]-out_pts_pers[1][0])//3),int(min(out_pts_pers[0][1],out_pts_pers[1][1])+abs(out_pts_pers[0][1]-out_pts_pers[1][1])//3)), cv2.FONT_HERSHEY_PLAIN, 1.5, (0,0,255), 1,cv2.LINE_AA)
    cv2.line(img_temp,tuple(out_pts_pers[0]),tuple(out_pts_pers[2]),(0,0,255),1)
    cv2.putText(img_temp, f"h=2.04m", (int(min(out_pts_pers[0][0],out_pts_pers[2][0])+abs(out_pts_pers[0][0]-out_pts_pers[2][0])//2)-110,int(min(out_pts_pers[0][1],out_pts_pers[2][1])+abs(out_pts_pers[0][1]-out_pts_pers[2][1])//2)), cv2.FONT_HERSHEY_PLAIN, 1.5, (0,0,255), 1,cv2.LINE_AA)
    cv2.line(img_temp,tuple(out_pts_pers[3]),tuple(out_pts_pers[1]),(0,0,255),1)
    cv2.line(img_temp,tuple(out_pts_pers[2]),tuple(out_pts_pers[3]),(0,0,255),1)
    cv2.putText(img_temp, "Puntos de perspectiva seleccionados:" , (0,18), cv2.FONT_HERSHEY_PLAIN, 1.5, (0,255,0), 1,cv2.LINE_AA)
    cv2.putText(img_temp, "Dimensiones de la ventana de la izquierda: 0.53m x 1.16m" , (0,18+14), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,255), 1,cv2.LINE_AA)
    cv2.putText(img_temp, "Dimensiones de la ventana de la derecha: 0.6m x 0.91m" , (0,18+14*2), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,255), 1,cv2.LINE_AA)
    cv2.putText(img_temp, "Dimensiones del calefactor: 0.34m x 0.60m" , (0,18+14*3), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,255), 1,cv2.LINE_AA)
    cv2.putText(img_temp, "Dimensiones del tomacorrientes: 0.13m x 0.08m" , (0,18+14*4), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,255), 1,cv2.LINE_AA)
    cv2.putText(img_temp, "Presione una tecla para continuar" , (0,oh-5), cv2.FONT_HERSHEY_PLAIN, 1.5, (0,255,255), 2,cv2.LINE_AA)
    cv2.namedWindow('Original Image')
    cv2.imshow("Original Image",img_temp)
    cv2.waitKey(0)
    cv2.destroyWindow("Original Image")

cv2.namedWindow('image')
cv2.setMouseCallback('image', rect_dist)
while(1):
    if draw:
        cv2.imshow('image', img)
        draw = False
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('r'):
        if mode_pers:
            img = orig.copy()
            cache = orig.copy()
            out_pts_pers=[]
            count_pers=4
            draw=True
        else:
            img = out_img.copy()
            cache = out_img.copy()
            dist_pts=[]
            count_dist=2
            dist_pts_2=[]
            count_dist_2=2
            draw=True
    elif k == ord('c'):
        if not hardcode:
            mode_pers=True
            img = orig.copy()
            cache = orig.copy()
            out_pts_pers=[]
            count_pers=4
            draw=True

cv2.destroyAllWindows()
