import cv2
import numpy

#Leo la imagen
img = cv2.imread('files/ej2_img.png', 0)

img2=[]
temp=[]
threshold=250

for row  in img: 
    for cols in row:
        if cols < threshold:
            temp.append(0)      #Si el color es mas oscuro que el threshold coloco un 0, o un pixel negro
        else:
            temp.append(255)    #Si el color es mas claro que el threshold coloco un 255, o un pixel blanco
    img2.append(temp)
    temp=[]
    

cv2.imwrite('files/ej2_out.png', numpy.array(img2))
