#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math as ma

img_path = "files/ej5_img.jpg"
orig = cv2.imread(img_path)  

def transf_eucl(a,tx,ty,im_orig):
    '''
        Esta función realiza la transformación euclideana de la imagen pasada por parámetro
        Inputs:
            a = Ángulo
            tx, ty = Traslación en X y en Y
            im_orig = imagen a transformar
    '''
    ang=a*ma.pi/180     #Grados a radianes
    h, w, ch = im_orig.shape
    tf_img = np.zeros((h-1,w-1,ch),np.uint8) #Declaro una matriz vacia para la matriz transformada
    trf_mat=np.float32([[ma.cos(ang),-ma.sin(ang)],[ma.sin(ang),ma.cos(ang)]])  #Matriz de transformación

    #Aplico la transformación con producto matricial.
    for i in range(h):
        for j in range(w):
            p_mat=np.array([[i],[j]])
            prod_mat=np.matmul(trf_mat,p_mat)
            iy=round(prod_mat[0][0])
            ix=round(prod_mat[1][0])
            if (ix+tx >= 0 and iy+ty >= 0) and (ix+tx < w and iy+ty < h):
                tf_img[int(ty+iy-1)][int(tx+ix-1)][:]=im_orig[i][j][:]
    return tf_img


img = orig.copy()
cache = img.copy()
drawing = False      # true if mouse is pressed
jx , jy = -1 , -1
pos_x = [-1,-1]
pos_y = [-1,-1]

#Mismo código del ej4
def draw_circle (event , x , y , flags , param ) :
    global jx , jy , drawing , mode, img, cache, pos_x, pos_y
    if event == cv2.EVENT_LBUTTONDOWN:
        img = orig.copy()
        cache = orig.copy()
        drawing = True
        jx , jy = x , y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            img = cache.copy()
            cache = img.copy()
            cv2.rectangle(img, (jx,jy), (x,y), (0,255,0), 1)
        else:
            img = cache.copy()
            cache = img.copy()
            cv2.circle(img, (x,y), 5, (0,0,255), -1)

    elif event == cv2 .EVENT_LBUTTONUP:
        drawing = False
        cv2.rectangle(img, (jx,jy), (x,y), (0,255,255), 2)
        cache = img.copy()
        pos_x = [jx,x]
        pos_y = [jy,y]

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('g'):
        if pos_x[0]>0:
            cv2.imwrite('files/ej5_out.png', orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:])  #Igual que el ej4
    elif k == ord('e'):
        if pos_x[0]>0:
            #Configurable:
                angu = 180  #Angulo a transformar
                despl_x = int( abs(pos_x[0]-pos_x[1]) * 1)  #tx
                despl_y = int( abs(pos_y[0]-pos_y[1]) * 1)  #ty
                myfile = orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:] #Porción de la imagen a transformar
                cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(angu,despl_x,despl_y,myfile))
            #Imagen rotada 180º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(180, int(abs(pos_x[0]-pos_x[1])), int(abs(pos_y[0]-pos_y[1])), myfile))
            #Imagen Rotada 225º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(225, int(abs(pos_x[0]-pos_x[1])*1.2), int(abs(pos_y[0]-pos_y[1])*0.5), myfile))
            #Imagen Rotada 45º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(45, int(abs(pos_x[0]-pos_x[1])*-0.22), int(abs(pos_y[0]-pos_y[1])*0.55), myfile))

    elif k == ord('r'):
        img = orig.copy()
        cache = orig.copy()
        pos_x=[-1,-1]
        pos_y=[-1,-1]
cv2.destroyAllWindows()
