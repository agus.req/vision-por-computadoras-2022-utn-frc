#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Los puntos se seleccionan de izquierda a derecha y de arriba a abajo.

import cv2
import numpy as np
import math as ma

img_path = "files/ej8_img.jpg"
orig = cv2.imread(img_path)  

def persp():
    '''
        Esta función realiza la transformación perspectiva de la imagen.
        No necesitamos entradas ya que utilizamos variables globales (mal)
    '''
    global img, cache, oh, ow, out_img, out_pts_pers

    #Pongo los puntos de una forma mas facil de leer
    x_min= out_pts_pers[0][0]
    x_max= out_pts_pers[3][0]
    y_min= out_pts_pers[0][1]
    y_max= out_pts_pers[3][1]

    #Muevo la imagen a la esquina superior izquierda
    x_max-=x_min
    x_min=0
    y_max-=y_min
    y_min=0

    #in_pts = np.float32([[0,0], [ow-1,0], [0,oh-1], [ow-1,oh-1]])
    in_pts = np.float32([out_pts_pers[0], out_pts_pers[1], out_pts_pers[2], out_pts_pers[3]])   #Los puntos de entrada son los puntos que seleccioné
    out_pts = np.float32([[x_min,y_min],[x_max,y_min],[x_min,y_max],[x_max,y_max]]) #Los puntos de salida son la esquina superior izquierda y el ancho y alto de la imagen recortada
    M = cv2.getPerspectiveTransform(in_pts,out_pts)

    img2 = cv2.resize(orig,(ow,oh))
    out_img = cv2.warpPerspective(img2, M, (x_max,y_max))   #"Enderezo" la imagen
    cv2.imshow('image_out',out_img)


img = orig.copy()
cache = img.copy()
out_pts_pers=[]
count_pers=4
oh, ow, och = orig.shape
mode_pers = False
is_out_img = False

def draw_circle (event , x , y , flags , param ) :
    global mode_pers, img, cache, count_pers, out_pts_pers
    if event == cv2.EVENT_LBUTTONDOWN:
        if mode_pers:
            #Agrego la posición del mouse al hacer click hasta tener 4 valores, y luego llamo a la transformación perspectiva (Podría haber utilizado len(count_pers) igual que en el otro ejercicio, pero de vuelta, me pudo mas escribir como si fuera C)
            if (count_pers > 0):
                out_pts_pers.append([x,y])
                count_pers-=1
                cv2.circle(img, (x,y), 5, (0,255,0), -1)
                if (count_pers==2):
                    cv2.line(img,tuple(out_pts_pers[0]),tuple(out_pts_pers[1]),(0,0,255),1)
                elif(count_pers==1):
                    cv2.line(img,tuple(out_pts_pers[0]),tuple(out_pts_pers[2]),(0,0,255),1)
                elif (count_pers == 0):
                    cv2.line(img,tuple(out_pts_pers[3]),tuple(out_pts_pers[1]),(0,0,255),1)
                    cv2.line(img,tuple(out_pts_pers[2]),tuple(out_pts_pers[3]),(0,0,255),1)
                    persp()
                cache = img.copy()
            
    elif event == cv2.EVENT_MOUSEMOVE:
        img = cache.copy()
        cache = img.copy()
        if mode_pers:
            cv2.circle(img, (x,y), 5, (0,255,255), -1)
        else:
            cv2.circle(img, (x,y), 5, (0,0,255), -1)

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('r'):
        #Reinicio la imagen, el contador y el array.
        img = orig.copy()
        cache = orig.copy()
        out_pts_pers=[]
        count_pers=4
        if(cv2.getWindowProperty('image_out', cv2.WND_PROP_VISIBLE)):
            cv2.destroyWindow('image_out')
    elif k == ord('h'):
        out_pts_pers=[]
        count_pers=4
        img = orig.copy()
        cache = orig.copy()
        mode_pers = not mode_pers
        if (cv2.getWindowProperty('image_out', cv2.WND_PROP_VISIBLE)):
            cv2.destroyWindow('image_out')
cv2.destroyAllWindows()
