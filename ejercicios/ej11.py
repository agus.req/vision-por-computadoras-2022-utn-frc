#! /usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2

MIN_MATCH_COUNT = 10

img1 = cv2.imread('files/ej11_img1.jpg')
img2 = cv2.imread('files/ej11_img2.jpg')
img_h,img_w = img1.shape[:2]

sift = cv2.SIFT_create()
kp1,des1 = sift.detectAndCompute(img1,None)
kp2,des2 = sift.detectAndCompute(img2,None)

matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)

# Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m, n in matches:
    if m.distance < 0.7 * n.distance:
        good.append(m)

if(len(good) > MIN_MATCH_COUNT):
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)


H, mask = cv2.findHomography(dst_pts,src_pts,cv2.RANSAC,5.0) # Computamos la homografía con RANSAC
wimg2 = cv2.warpPerspective(img2,H,(img_w,img_h))# Aplicamos la transformación perspectiva H a la imagen 2

# Mezclamos ambas imágenes
alpha = 0.5
blend = cv2.addWeighted(img1,alpha,wimg2,1-alpha,1)
#blend = np.array(wimg2*alpha+img1*(1-alpha),dtype=np.uint8)




cv2.imshow('Imagen',blend)
cv2.waitKey(0)

matchesMask = mask.ravel().tolist()
pts = np.float32([ [0,0],[0,img_h-1],[img_w-1,img_h-1],[img_w-1,0] ]).reshape(-1,1,2)
dst = cv2.perspectiveTransform(pts,H)
img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)

draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                singlePointColor=None,
                matchesMask=matchesMask, #draw only in liers
                flags = 2)
img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

cv2.imshow('Matches',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
