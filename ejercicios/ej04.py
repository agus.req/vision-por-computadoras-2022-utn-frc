#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

#Leo la imagen
img_path = "files/ej4_img.png"
img = cv2.imread(img_path)  
cache = img.copy()
orig = img.copy()

#Declaro variables
drawing = False
ix , iy = -1 , -1
pos_x = [0,0]
pos_y = [0,0]

def draw_circle (event , x , y , flags , param ) :
    global ix , iy , drawing , mode, img, cache, pos_x, pos_y
    #Si un boton es presionado reinicio la imagen a mostrar y el caché
    if event == cv2.EVENT_LBUTTONDOWN:
        img = orig.copy()
        cache = orig.copy()
        drawing = True
        ix , iy = x , y
    #Si estoy moviendo el mouse que la imagen sea la tomada por el caché (la original) y la del caché pasa a la imagen
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            img = cache.copy()
            cache = img.copy()
            cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)    #Si estoy presionando el click que dibuje un rectangulo
        else:
            img = cache.copy()
            cache = img.copy()
            cv2.circle(img, (x,y), 5, (0,0,255), -1)    #Si no lo estoy haciendo que me dibuje un circulo en la posicion del mouse

    elif event == cv2 .EVENT_LBUTTONUP:
        #Al soltar el mouse que se dibuje un rectangulo en las posiciones marcadas y se devuelven las posiciones de inicio y fin del rectangulo
        drawing = False
        cv2.rectangle(img, (ix,iy), (x,y), (0,0,255), 2)
        cache = img.copy()
        pos_x = [ix,x]
        pos_y = [iy,y]
        #print(f"{pos_x} {pos_y}")

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27 :
        break
    elif k == ord('g'):
        cv2.imwrite('files/ej4_out.png', orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:])  #Guardo la imagen en la posicion deseada
    elif k == ord('r'):
        img = orig.copy()
        cache = orig.copy()
        pos_x=[0,0]
        pos_y=[0,0]
cv2.destroyAllWindows()
