#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math as ma

img_path = "files/ej7_img.jpg"
orig = cv2.imread(img_path)  
img_path2 = "files/ej7_img2.jpg"
orig2 = cv2.imread(img_path2)
def escalar(a, tx, ty, im_orig, s):
    '''
        Esta función realiza la transformación euclideana y escalado de la imagen pasada por parámetro
        Inputs:
            a = Ángulo
            tx, ty = Traslación en X y en Y
            im_orig = imagen a transformar
            s = multiplicador de escala (1 = tamaño original)
    '''
    ang=a*ma.pi/180
    h, w, ch = im_orig.shape
    tf_img = np.zeros((round(h*s),round(w*s),ch),np.uint8)
    trf_mat=np.float32([[s*ma.cos(ang),-s*ma.sin(ang)],[s*ma.sin(ang),s*ma.cos(ang)]])

    #Aplico la transformación con producto matricial.
    for i in range(h):
        for j in range(w):
            p_mat=np.array([[i],[j]])
            prod_mat=np.matmul(trf_mat,p_mat)
            iy=round(prod_mat[0][0])
            ix=round(prod_mat[1][0])
            #Ya que cada pixel pasa a ser 4 pixeles en el caso de duplicar el tamaño de la imagen, debo escribir el mismo pixel dependiendo el valor de escala
            for k in range(round(s)+1):
                for l in range(round(s)+1):
                    if (ix+tx-k >= 0 and iy+ty-l >= 0) and (ix+tx-k < round(w*s) and iy+ty-l < round(h*s)):
                        tf_img[int(ty+iy-l)][int(tx+ix-k)][:]=im_orig[i][j][:]
                
    return tf_img

def transf_eucl(a,tx,ty,im_orig):
    '''
        Esta función realiza la transformación euclideana de la imagen pasada por parámetro
        Inputs:
            a = Ángulo
            tx, ty = Traslación en X y en Y
            im_orig = imagen a transformar
    '''
    ang=a*ma.pi/180
    h, w, ch = im_orig.shape
    tf_img = np.zeros((h-1,w-1,ch),np.uint8)
    trf_mat=np.float32([[ma.cos(ang),-ma.sin(ang)],[ma.sin(ang),ma.cos(ang)]])
    for i in range(h):
        for j in range(w):
            p_mat=np.array([[i],[j]])
            prod_mat=np.matmul(trf_mat,p_mat)
            iy=round(prod_mat[0][0])
            ix=round(prod_mat[1][0])
            if (ix+tx >= 0 and iy+ty >= 0) and (ix+tx < w and iy+ty < h):
                tf_img[int(ty+iy-1)][int(tx+ix-1)][:]=im_orig[i][j][:]
    return tf_img

def afin():
    '''
        Esta función realiza la transformación afin (warp) de la imagen.
        No se necesitan parametros ya que en este caso utilizamos variables globales. (mal)
    '''
    global img, img2, cache, oh, ow, out_img, out_pts
    in_pts = np.float32([[0,0], [ow-1,0], [0,oh-1]])    #Puntos de entrada (esquinas de la imagen)
    out_pts = np.float32([out_pts[0], out_pts[1], out_pts[2]])  #Puntos en los que se quiere colocar la imagen (ingresado por clicks)
    M = cv2.getAffineTransform(in_pts,out_pts)  #Matriz de transformación

    img2 = cv2.resize(img2,(ow,oh)) #Para poder pegarla encima de la otra necesito que sus tamaños sean iguales
    out_img = cv2.warpAffine(img2, M, (ow,oh))  #Transformo la imagen
    
    #Creo una máscara de la imagen para pegarla (En el ej10 lo hago de una forma mas eficiente que es utilizando polígonos)
    img2bw = np.zeros([oh,ow],dtype=np.uint8)
    img2bw.fill(255)
    img2bw = cv2.resize(img2bw,(ow,oh))
    out_img2 = cv2.warpAffine(img2bw, M, (ow,oh))
    _, mask = cv2.threshold(out_img2[:,:], 0, 255, cv2.THRESH_BINARY|cv2.THRESH_OTSU)

    #Copio la imagen donde la mascara sea blanca
    img=orig.copy()
    img[np.where(mask == 255)] = out_img[np.where(mask == 255)]
    cache = img.copy()


img = orig.copy()
img2 = orig2.copy()
cache = img.copy()
out_pts=[]
count=3
oh, ow, och = orig.shape
drawing = False      # true if mouse is pressed
jx , jy = -1 , -1
pos_x = [-1,-1]
pos_y = [-1,-1]
mode = False

#Todo el codigo es igual al del ej anterior excepto la parte de selección de puntos
def draw_circle (event , x , y , flags , param ) :
    global jx , jy , drawing , mode, img, cache, pos_x, pos_y, out_pts, count
    if event == cv2.EVENT_LBUTTONDOWN:
        if not mode:
            img = orig.copy()
            cache = orig.copy()
            drawing = True
            jx , jy = x , y
        else:
            #Si hago click y estoy en el modo de transformación afin guardo los puntos clickeados hasta que haya clickeado 3 veces y ahí recien aplico la transformacion (Podría haber utilizado len(out_pts) en vez de un contador pero me pudo mas pensar como si fuera C en vez de python)
            if (count > 0):
                out_pts.append([x,y])
                count-=1
                cv2.circle(img, (x,y), 5, (0,255,0), -1)
                cache = img.copy()
                if (count == 0):
                    afin()
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            img = cache.copy()
            cache = img.copy()
            cv2.rectangle(img, (jx,jy), (x,y), (0,255,0), 1)
        else:
            img = cache.copy()
            cache = img.copy()
            if mode:
                cv2.circle(img, (x,y), 5, (255,255,0), -1)
            else:
                cv2.circle(img, (x,y), 5, (0,0,255), -1)

    elif event == cv2 .EVENT_LBUTTONUP:
        if not mode:
            drawing = False
            cv2.rectangle(img, (jx,jy), (x,y), (0,0,255), 2)
            cache = img.copy()
            pos_x = [jx,x]
            pos_y = [jy,y]

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27 :
        break
    elif k == ord('g'):
        if pos_x[0]>0:
            cv2.imwrite('files/ej7_out.png', orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:])
    elif k == ord('e'):
        if pos_x[0]>0:
            #Configurable:
                angu = 180
                despl_x = int( abs(pos_x[0]-pos_x[1]) * 1)
                despl_y = int( abs(pos_y[0]-pos_y[1]) * 1)
                myfile = orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:]
                cv2.imwrite('files/ej7_out_eucl.png', transf_eucl(angu,despl_x,despl_y,myfile))
            #Imagen rotada 180º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(180, int(abs(pos_x[0]-pos_x[1])), int(abs(pos_y[0]-pos_y[1])), myfile))
            #Imagen Rotada 225º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(225, int(abs(pos_x[0]-pos_x[1])*1.2), int(abs(pos_y[0]-pos_y[1])*0.5), myfile))
            #Imagen Rotada 45º
                #cv2.imwrite('files/ej5_out_eucl.png', transf_eucl(45, int(abs(pos_x[0]-pos_x[1])*-0.22), int(abs(pos_y[0]-pos_y[1])*0.55), myfile))
    elif k == ord('s'):
        if pos_x[0]>0:
            #Configurable:
                escala = 0.1        ## Funciona con valores menores a 1 como por ejemplo 0.5 o 0.2
                angu = 0
                despl_x = int(escala * abs(pos_x[0]-pos_x[1]) * 0)
                despl_y = int(escala * abs(pos_y[0]-pos_y[1]) * 0)
                myfile = orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:]
                cv2.imwrite('files/ej7_out_scale.png', escalar(angu,despl_x,despl_y,myfile,escala))
            #Imagen rotada 180º
                #cv2.imwrite('files/ej6_out_scale.png', escalar(180, int(escala*abs(pos_x[0]-pos_x[1])), int(escala*abs(pos_y[0]-pos_y[1])), myfile, escala))
            #Imagen Rotada 225º
                #cv2.imwrite('files/ej6_out_scale.png', escalar(225,int(abs(pos_x[0]-pos_x[1])*1.2),int(abs(pos_y[0]-pos_y[1])*0.5),myfile))
            #Imagen Rotada 45º
                #cv2.imwrite('files/ej6_out_scale.png', escalar(45,int(abs(pos_x[0]-pos_x[1])*-0.15),int(abs(pos_y[0]-pos_y[1])*0.55),myfile))
    elif k == ord('r'):
        img = orig.copy()
        cache = orig.copy()
        pos_x=[-1,-1]
        pos_y=[-1,-1]
        out_pts=[]
        count=3
    elif k == ord('a'):
        out_pts=[]
        count=3
        img = orig.copy()
        cache = orig.copy()
        mode = not mode
cv2.destroyAllWindows()
