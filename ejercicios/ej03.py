#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import cv2

if(len(sys.argv)>1):
    filename=sys.argv[1]
else:
    print('Pass a filename as first argument')
    sys.exit(0)

#Creo el objeto de lectura de video
cap = cv2.VideoCapture(filename)

#Declaro el codec del video
fourcc = cv2.VideoWriter_fourcc(*'DIVX')

# Consigo el alto y ancho del video y luego lo ingreso en el tuple de framesize
ancho=int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
alto=int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
framesize = (ancho,alto) 

# Consigo los FPS del video y luego en delay hago una division entera para conseguir el retardo en ms.
fps = cap.get(cv2.CAP_PROP_FPS) 
delay =int((1/fps)*1000)

#Lo guardamos con los mismos fps que lo leemos
out = cv2.VideoWriter(sys.argv[1][:-4]+'_output.avi', fourcc , fps, framesize,0) 

#Mostramos por pantalla los frames del video
while(cap.isOpened()):
    ret , frame = cap.read()
    if ret:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imshow('Image gray' , gray)
        out.write(gray)
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
