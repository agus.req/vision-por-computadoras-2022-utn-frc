import cv2
import numpy as np
from cv2 import aruco
import glob
import os

CAMERA_PORT = 0     #Declarar aquí el puerto de la camara.

def calibrate():
    '''
        Esta función calibra la camara en el hipotetico caso de que no existan imagenes de calibración de la misma.
        Al terminar la calibración guarda las imagenes de calibración para no tener que hacerlo nuevamente.
    '''
    global mtx, dist, height, width
    CHECKERBOARD = (6, 9)
    criteria = (cv2.TERM_CRITERIA_EPS +
                cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    threedpoints = []
    twodpoints = []
    objectp3d = np.zeros((1, CHECKERBOARD[0] * CHECKERBOARD[1],3), np.float32)
    objectp3d[0, :, :2] = np.mgrid[0:CHECKERBOARD[0],0:CHECKERBOARD[1]].T.reshape(-1, 2)
    prev_img_shape = None

    images = glob.glob('calib/*.jpg')

    if len(images)==0:
        img = None
        fname = "aruco/checkerboard.png"
        img = cv2.imread(fname, 0)
        if img is None:
            print("#"*100)
            print(" >> ERROR >> No existe la imagen de calibración en el directorio ./aruco/ ".center(100,"#"))
            print(" >> ERROR >> No se encuentra el archivo ./aruco/checkerboard.png ".center(100,"#"))
            print(" >> ERROR >> Asegurese de descargar la carpeta aruco con sus contenidos y colocarla en la raiz ".center(100,"#"))
            print("#"*100)
            quit()
        counter = 3
        try:
            os.mkdir('calib/')
        except:
            pass
        while(True):
            ret, frame = cam_cap.read()
            orig_frame = frame.copy()
            if ret:
                cv2.imshow("CALIBRACION", img)

                cv2.putText(frame, f"(S) Guardar imagenes para calibracion [{3-counter}/3]", (0,12), cv2.FONT_HERSHEY_PLAIN, 1, (0,0,255),1,cv2.LINE_AA)
                cv2.putText(frame, "Presione la tecla S para guardar una imagen de calibracion", (0,height-2), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0),1,cv2.LINE_AA)
                cv2.imshow("CAMARA", frame)

                k = cv2.waitKey(delay-20) & 0xFF
                if k == ord('s') or k == ord('S'):
                    cv2.imwrite(f'calib/{4-counter}_calib.jpg', orig_frame)
                    counter-=1
                if k==27:
                    quit()
            if counter==0:
                cv2.destroyAllWindows()
                break

    images = glob.glob('calib/*.jpg')

    for filename in images:
        image = cv2.imread(filename)
        grayColor = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(
                        grayColor, CHECKERBOARD,
                        cv2.CALIB_CB_ADAPTIVE_THRESH
                        + cv2.CALIB_CB_FAST_CHECK +
                        cv2.CALIB_CB_NORMALIZE_IMAGE)

        if ret == True:
            threedpoints.append(objectp3d)
            corners2 = cv2.cornerSubPix(grayColor, corners, (11, 11), (-1, -1), criteria)
            twodpoints.append(corners2)

    if not len(threedpoints)>0:
        try:
            os.remove('calib/1_calib.jpg')
            os.remove('calib/2_calib.jpg')
            os.remove('calib/3_calib.jpg')
        except:
            pass
        print("#"*80)
        print(" >> ERROR >> Las imagenes de calibraciosn agregadas no son validas ".center(80,"#"))
        print("#"*80)
        calibrate()
    else:
        mtx, dist = cv2.calibrateCamera(threedpoints, twodpoints, grayColor.shape[::-1], None, None)[1:3]

#Abro la camara
cam_cap = cv2.VideoCapture(CAMERA_PORT)
fourcc = cv2.VideoWriter_fourcc(*'XVID')

#Parámetros de la cámara.
width=int(cam_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height=int(cam_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
framesize = (width,height) 
fps = cam_cap.get(cv2.CAP_PROP_FPS)
if fps==0:
    print("#"*60)
    print(" >> ERROR >> Camara no detectada (FPS = 0) ".center(60,"#"))
    print("#"*60)
    quit()
else:
    delay =int((1/fps)*1000)

#Cargo todas los videos de la carpeta "aruco"
videos = glob.glob('aruco/*.mp4')
vid_max_frame=[]
vid_next_frame=[]
ar_vid=[]
for i,fname in enumerate(videos):
    ar_vid.append(cv2.VideoCapture(fname))   
    vid_max_frame.append(ar_vid[i].get(cv2.CAP_PROP_FRAME_COUNT))
    

if not (len(ar_vid)>0):
    print("#"*80)
    print(" >> ERROR >> El directorio ./aruco/ esta vacio ".center(80,"#"))
    print("Agregue imagenes .jpg en el directorio ./aruco/".center(80,"#"))
    print("#"*80)
    quit()
else:
    calibrate()
    print("#"*106)
    print("  BIENVENIDO  ".center(106,"#")+"\n"+" Este programa de aruco se encarga de imprimir un video en arucos de 6x6. ".center(106,"#")+"\n"+
        " Dependiendo el id del aruco será el video que se mostrará por ventana. ".center(106,"#")+"\n"+
        " Para agregar videos, agrege archivos .mp4 a la dirección ./aruco/*.mp4 " .center(106,"#") )
    print("#"*106)
    print(" Si presiona la tecla M se cambiará el modo de vista de imagen, de plano, a plano normal y viceversa. ".center(106,"#")+"\n"+
        " Si presiona la tecla B se mostrará u ocultarán los bordes del aruco. ".center(106,"#") )
    print("#"*106)
    print("Saludos. Requejo Agustín.")


'''
#La función draw se encarga de colocar la imagen en la pantalla
#Recibe como parámetros la imagen (frame), los puntos con los que se desea transformar y el id del aruco.
#Si los puntos son las esquinas del aruco, la imagen se pega en el plano del aruco
#Si los puntos son las puntas de los vectores, la imagen se pega de tal forma de ser ortonormal al plano del aruco.
def draw(img, pts,ids):
    global height,width
    pts = np.int32(pts).reshape(-1,2)
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1], [0,height-1]])
    out_pts = np.float32([pts])
    M=cv2.getPerspectiveTransform(in_pts,out_pts)
    warped_image = cv2.warpPerspective(ar_img[ids%len(ar_img)], M, (width,height))

    mask = np.zeros([height,width], dtype=np.uint8)
    cv2.fillConvexPoly(mask, np.int32([pts]), (255, 255, 255), cv2.LINE_AA)
        
    img[np.where(mask == 255)] = warped_image[np.where(mask == 255)]
    return img
'''

#La función gif_draw se encarga de colocar la imagen del frame del video en la pantalla
#Recibe como parámetros la imagen (frame de la camara), los puntos con los que se desea transformar y el id del aruco.
#Si los puntos son las esquinas del aruco, la imagen se pega en el plano del aruco
#Si los puntos son las puntas de los vectores, la imagen se pega de tal forma de ser ortonormal al plano del aruco.
def gif_draw(img, pts, ids):
    '''
    '''
    global height,width,max_frame,next_frame
    pts = np.int32(pts).reshape(-1,2)
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1], [0,height-1]])
    out_pts = np.float32([pts])
    M=cv2.getPerspectiveTransform(in_pts,out_pts)
    
    cap = ar_vid[ids%len(ar_vid)]
    max_frame = vid_max_frame[ids%len(ar_vid)]
    next_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
    
    if next_frame >= max_frame :
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    ret, img2warp = cap.read()
    if not ret:
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        ret, img2warp = cap.read()
        if not ret:
            img2warp = np.zeros([height,width],dtype=np.uint8)

    img2warp = cv2.resize(img2warp, (width,height))
    warped_image = cv2.warpPerspective(img2warp, M, (width,height))

    mask = np.zeros([height,width], dtype=np.uint8)
    cv2.fillConvexPoly(mask, np.int32([pts]), (255, 255, 255), cv2.LINE_AA)
        
    img[np.where(mask == 255)] = warped_image[np.where(mask == 255)]

    return img


def proc_aruco():
    '''
        Process aruco
        Esta función se llama cada vez que se lea un frame de la cámara
        Esta función procesa los arucos encontrados en el frame, guarda las posiciones de sus esquinas y la de las esquinas del plano ortonormal y llama a la función que aplica el frame del video en el frame de la camara.
    '''
    global frame, framebw, tags, mtx, dist
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    framebw = cv2.merge([gray,gray,gray])
    framebwcache = framebw.copy()
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    aruco.drawDetectedMarkers(frame.copy(), corners, ids)

    #La declaración de los puntos referenciados, el centro del aruco es [0,0,0] y el sistema de coordenadas es [x,y,z]
    axis = np.float32([ [-0.5,0,1], [0.5,0,1], [0.5,0,0], [-0.5,0,0] ])
    
    if ids is not None:
        rvec, tvec = aruco.estimatePoseSingleMarkers(corners, 1, mtx, dist)[:2]
        (rvec-tvec).any() 

        for i in range(rvec.shape[0]):
            i = rvec.shape[0] - i - 1

            #Consigo los puntos de los vectores en la imagen referidos al centro del aruco.
            imgpts, _ = cv2.projectPoints(axis, rvec[i], tvec[i], mtx, dist)
            
            if not mode:
                frame = gif_draw(frame,corners[i][0],ids[i][0])
            else:
                frame = gif_draw(frame,imgpts,ids[i][0])

            if borders:
                aruco.drawDetectedMarkers(frame, corners)
            
    cv2.putText(frame, "(M) Modo ortogonal: "+tags[mode], (0,12), cv2.FONT_HERSHEY_PLAIN, 1, (0,0,255),1,cv2.LINE_AA)
    cv2.putText(frame, "(B) Mostrar bordes: "+tags[borders], (0,25), cv2.FONT_HERSHEY_PLAIN, 1, (0,0,255),1,cv2.LINE_AA)

#out = cv2.VideoWriter(filename[:-4]+'_output.avi', fourcc , fps, framesize) #Lo guardamos con los mismos fps que lo leemos
#cv2.namedWindow('Image', flags=cv2.WINDOW_GUI_EXPANDED) 
#cv2.resizeWindow('Image', 1366, 768)
mode = False
borders = False
tags={True:"ON",False:"OFF"}

while(cam_cap.isOpened()):
    ret, frame = cam_cap.read()
    if ret:
        proc_aruco()
        cv2.imshow('Image',frame)
        #out.write(frame)
        k = cv2.waitKey(delay-20) & 0xFF
        if k == 27:
            break
        elif k == ord('m') or k == ord('M'):
            mode = not mode
        elif k == ord('b') or k == ord('B'):
            borders = not borders
    else:
        break

cv2.destroyAllWindows()
